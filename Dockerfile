FROM centos:8

RUN yum install java-11-openjdk -y
RUN curl -Ls https://git.io/sbt > /bin/sbt && chmod 0755 /bin/sbt
