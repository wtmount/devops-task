object Main {
  def square(i: Int): Int = {
    i * i
  }

  def cube(i: Int): Int = {
    i * i * i
  }

  def double(i: Int): Int = {
    i * 2
  }
}
