import org.scalatest.flatspec.AnyFlatSpec

class MainTest extends AnyFlatSpec {

  behavior of "MainTest"

  it should "return square of a passed number" in {
    assert(Main.square(5) == 25)
  }

  it should "return cube of a passed number" in {
    assert(Main.cube(5) == 125)
  }

  it should "double passed number" in {
    assert(Main.double(5) == 10)
  }

}
